import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthendicationService } from '../authendication.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  loginData: any = {};
  errorText: any;

  constructor(
    private authendicationService: AuthendicationService,
    private router: Router
  ) { }

  login(data: any) {
    if (data.email === 'admin@gmail.com') {
      this.router.navigateByUrl('/admin');
    } else if (data.email === 'trader@gmail.com') {
      this.router.navigateByUrl('/trader');
    } else {
      this.errorText = 'Please Check email or password';
    }
    // this.authendicationService.login(data)
    //   .subscribe((result) => {
    //     localStorage.setItem('user', JSON.stringify(result));
    //     console.log(result);
    //     // this.router.navigateByUrl('/');
    //   }, (error) => {

    //   });
  }

  signUp() {
    this.router.navigateByUrl('/login');

  }

  ngOnInit(): void {
  }

}

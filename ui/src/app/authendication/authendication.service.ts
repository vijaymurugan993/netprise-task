import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthendicationService {

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post(`${environment.apiUrl}api/login`, data);
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trader',
  templateUrl: './trader.component.html',
  styleUrls: ['./trader.component.scss']
})
export class TraderComponent implements OnInit {

  constructor(
    private router: Router
  ) { }


  logOut() {
    this.router.navigateByUrl('/sign-up');

  }

  ngOnInit(): void {
  }

}

'use strict'

const bcrypt = require('bcryptjs');

module.exports = {

    hashPassword: async(password) => {
        try {
            const salt = await bcrypt.genSaltSync(10);
            return await bcrypt.hash(password, salt);
        } catch (error) {
            throw new Error("hasing falid", error);
        }
    },

    comparePassword: async(inputPassword, hashedPassword) => {
        try {
            return await bcrypt.compare(inputPassword, hashedPassword);
        } catch (error) {
            throw new Error('comparing failed');
        }
    },

}
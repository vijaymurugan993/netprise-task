'use strict';
module.exports = (sequelize, DataTypes) => {
    const bid = sequelize.define('bid', {
        id: {
            type: DataTypes.STRING,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            autoIncrement: false
        },
        name: DataTypes.STRING,
        offer: DataTypes.FLOAT,
        finalizeOffer: DataTypes.FLOAT,
        status: DataTypes.BOOLEAN,
        reason: DataTypes.STRING,
        isActive: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        createdBy: DataTypes.STRING,
        updatedBy: DataTypes.STRING
    }, {});
    bid.associate = function(models) {
        // associations can be defined here
    };
    return bid;
};
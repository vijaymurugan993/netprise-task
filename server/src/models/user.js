'use strict';
module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        id: {
            type: DataTypes.STRING,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            autoIncrement: false
        },
        firstName: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        isActive: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        createdBy: DataTypes.STRING,
        updatedBy: DataTypes.STRING
    }, {});
    user.associate = function(models) {

        user.hasMany(models.bid);

    };
    return user;
};
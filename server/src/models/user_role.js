'use strict';
module.exports = (sequelize, DataTypes) => {
    const user_role = sequelize.define('user_role', {
        id: {
            type: DataTypes.STRING,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            autoIncrement: false
        },
        name: DataTypes.STRING,
        isActive: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        createdBy: DataTypes.STRING,
        updatedBy: DataTypes.STRING
    }, {});
    user_role.associate = function(models) {

        user_role.hasOne(models.user);

    };
    return user_role;
};
'use strict'

const Models = require('../models');

module.exports = {

    adminRole: () => {
        return new Promise(async(resolve, reject) => {
            let role = await Models.user_role.findOne({
                where: {
                    isActive: true,
                    id: process.env.admin
                },
                raw: true
            });

            if (role) {
                resolve(role.id);
            } else {
                reject(false)
            }
        })
    },

    userRole: () => {
        return new Promise(async(resolve, reject) => {
            let role = await Models.user_role.findOne({
                where: {
                    isActive: true,
                    id: process.env.user
                },
                raw: true
            });

            if (role) {
                resolve(role.id);
            } else {
                reject(false)
            }
        })
    }

}
'use strict'

module.exports = {
    errorHandling: (err, res) => {
        console.log(err)
        res.status(500).json(err.message);
        return;
    }
}
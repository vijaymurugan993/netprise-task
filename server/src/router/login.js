const express = require('express'),
    router = express.Router(),
    passport = require('passport');

router.post('/login', passport.authenticate('local', {}), (req, res) => {
    // console.log(req.user, 'dddd')
    res.status(200).json(req.user)
});

router.get('/logout', (req, res, next) => {
    req.logout();
    req.session.destroy();
    res.status(200).json(message.logout.logout)
});

module.exports = router;
'use strict'

module.exports = {

    user: require('./user'),
    login: require('./login'),
    bid: require('./bid')

}
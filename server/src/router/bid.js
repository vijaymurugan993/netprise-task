'use strict'

const express = require('express'),
    router = express.Router(),
    bid = require('../controller/bid');

router.post('/bid', bid.createBid);

router.get('/bid/:id', bid.getBid);

router.get('/bids', bid.getBids);

router.get('/user-bids/:userId', bid.getUsersBid);

router.put('/bid-reject', bid.rejectBid);

router.put('/bid-approve', bid.approveBid);

router.delete('/bid-delete/:id', bid.deleteBid);



module.exports = router
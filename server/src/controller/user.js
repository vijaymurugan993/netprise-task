"use strict";

const Models = require("../models"),
    utils = require("../utils/utils"),
    common = require("../common/common"),
    messages = require("../messages/result_messages.json"),
    error = require("../error/error");

module.exports = {

    createUser: async(req, res, next) => {
        try {
            req.body.password = await utils.hashPassword(req.body.password);
            if (req.body.type === 'user') {
                req.body.userRoleId = await common.userRole();
                Models.user.create(req.body)
                    .then((result) => {
                        res.status(200).json(result);
                    })
                    .catch((err) => {
                        error.errorHandling(err, res);
                    });
            } else {
                req.body.userRoleId = await common.adminRole();
                Models.user.create(req.body)
                    .then((result) => {
                        res.status(200).json(result);
                    })
                    .catch((err) => {
                        error.errorHandling(err, res);
                    });
            }
        } catch (error) {
            next(error);
        }
    }

}
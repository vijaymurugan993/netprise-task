"use strict";

const Models = require("../models"),
    messages = require("../messages/result_messages.json"),
    error = require("../error/error");

module.exports = {

    createBid: (req, res, next) => {
        try {
            Models.bid.create(req.body)
                .then((result) => {
                    res.status(200).json(result);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    getBid: (req, res, next) => {
        try {
            Models.bid.findOne({
                    where: {
                        id: req.params.id
                    }
                }).then((result) => {
                    res.status(200).json(result);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    getBids: (req, res, next) => {
        try {
            Models.bid.findAll({
                    where: {
                        isActive: true
                    }
                }).then((result) => {
                    res.status(200).json(result);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    getUsersBid: (req, res, next) => {
        try {
            Models.user.findOne({
                    where: {
                        userId: req.params.userId,
                        isActive: true
                    },
                    include: [{
                        model: Models.bid,
                        required: true
                    }]
                }).then((result) => {
                    if (result) {
                        res.status(200).json(result);
                    } else {
                        res.status(200).json(messages.bid.notFound);
                    }
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    rejectBid: (req, res, next) => {
        try {
            Models.bid.update({
                    status: false
                }, {
                    where: {
                        id: req.body.id
                    }
                }).then(() => {
                    res.status(200).json(messages.bid.reject);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    approveBid: (req, res, next) => {
        try {
            Models.bid.update({
                    status: true
                }, {
                    where: {
                        id: req.body.id
                    }
                }).then(() => {
                    res.status(200).json(messages.bid.reject);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    deleteBid: (req, res, next) => {
        try {
            Models.bid.update({
                    isActive: false
                }, {
                    where: {
                        id: req.params.id
                    }
                }).then(() => {
                    res.status(200).json(messages.bid.delete);
                })
                .catch((err) => {
                    error.errorHandling(err, res);
                });
        } catch (error) {
            next(error);
        }
    },

    userOffer: (req, res, next) => {
        try {
            Models.bid.update({
                finalizeOffer: req.body.finalizeOffer,
                userId: ''
            })
        } catch (error) {
            next(error)
        }
    }

}
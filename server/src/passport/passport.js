const passport = require('passport'),
    localStrategy = require('passport-local').Strategy,
    JWT = require('jsonwebtoken');

const Models = require('../models'),
    message = require('../messages/result_messages.json'),
    utils = require('../utils/utils');

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser(async function(user, done) {
    done(null, user);
});

passport.use('local', new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: false
}, async(email, password, done) => {
    try {
        var user = await Models.user.findOne({
            where: {
                email: email
            }
        });

        if (!user) {
            return done(null, false, message.passport.notValid);
        }
        const isValid = await utils.comparePassword(password, user.dataValues.password);
        if (!isValid) {
            return done(null, false, message.passport.notValid);
        }
        if (!user.isActive) {
            return done(null, message.passport.verify);
        }
        if (isValid) {
            var token = JWT.sign({
                user
            }, 'mysecretKey', {
                expiresIn: 3600000
            });
            user.dataValues.formattedToken = token;
            return done(null, user);
        }


    } catch (error) {
        return done(error, false);
    }
}));
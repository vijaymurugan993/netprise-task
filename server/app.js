var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var passport = require('passport');
var cors = require('cors');
var session = require('express-session');

var app = express();

var userRouter = require('./src/router');
var message = require('./src/messages/result_messages.json')

// config
require('dotenv').config();
require('./src/passport/passport');

//session
app.use(session({
    key: 'session_cookie_name',
    secret: 'khjsdkjahsdajhasdam,nnsnad,',
    resave: false,
    saveUninitialized: false
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// external
app.use(passport.initialize());
app.use(passport.session());

app.use('/api', userRouter.user, userRouter.login, userRouter.bid);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).json(message.pageNotFound.notFound)
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    console.log(err)
    res.status(500).json(err.message);
    return;
});

module.exports = app;